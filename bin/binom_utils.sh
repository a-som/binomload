export LANG=en_US.UTF-8
export TERM=linux

BASE_DIR=/opt/binomload
LOGS_DIR=$BASE_DIR/logs
TEMP_DIR=$BASE_DIR/temp
CACHE_DIR=$BASE_DIR/tmp
BIN_DIR=$BASE_DIR/bin
bx_process_script=$BIN_DIR/binom-process
bx_sites_script=$BIN_DIR/binom-load

LOGS_FILE=$LOGS_DIR/load_menu.log
MENU_SPACER="------------------------------------------------------------------------------------"

# get ip address of host
get_ip_addr() {
  # get firt ip address for host
  ip -f inet -o addr show | cut -d\  -f 7 | cut -d/ -f 1 | grep -v '127\.0\.0\.1' | head -1
}

print_color_text(){
  _color_text="$1"
  _color_name="$2"
  _echo_opt="$3"
  [[ -z "$_color_name" ]] && _color_name='green'
  _color_number=38

  case "$_color_name" in
    green)    _color_number=32 ;;
    blue)     _color_number=34 ;;
    red)      _color_number=31 ;;
    cyan)     _color_number=36 ;;
    magenta)  _color_number=35 ;;
    *)        _color_number=39 ;;
  esac

  echo -en "\\033[1;${_color_number}m"
  echo $_echo_opt "$_color_text"
  echo -en "\\033[0;39m"
}

# save information in log file
print_log() {
  _log_message=$1
  _log_file=$2
  if [[ -n "$_log_file" ]]; then
    log_date=$(date +'%Y-%m-%dT%H:%M:%S')
    # exclude test domain
    printf "%-14s: %6d: %s\n" "$log_date" "$$" "$_log_message" >> $_log_file
  else
    printf "%-14s: %6d: %s\n" "$log_date" "$$" "$_log_message"
  fi
}

# set logo
get_logo(){
  logo="Binom load appliance"
  logov="Alfa Binom V0.3"
  echo -e "\t\t" $logo " version "$logov
}

print_header(){
  _header_text=$1
  echo -e '\t\t\t' "$_header_text"
  echo
}

print_verbose(){
  _verbose_type=$1
  _verbose_message=$2
  [[ -z $VERBOSE ]] && VERBOSE=0
  if [[ $VERBOSE -gt 0 ]]; then
    print_color_text "$_verbose_type" green -n
    echo ": $_verbose_message"
  fi
}

# error message for all possible menus
error_pick(){
  notice_message=`print_color_text "Ошибка ввода. Повторите попытку" red -n` ;
}

# print error message
# as we use cycles, must make sure that the user sees an error
print_message(){
    _input_message=${1}       # prompt in read output
    _print_message=${2}       # colored text like a notice
    _input_format=${3}        # can add option to read
    _input_key=${4}           # saved variable name
    _input_default=${5}       # default value for variable
    _read_input_key=
    _notset_input_key=0       # printf change empty string

    [[ -z "$_input_message" ]] && _input_message="Press ENTER for exit: "

    # print notice message
    [[ -n "$_print_message" ]] && print_color_text "$_print_message" blue -e
    echo

    # get variable value from user
    # -r If this option is given, backslash does not act as an escape character
    read $_input_format -r -p "$_input_message" _read_input_key
    if [[ -z "$_read_input_key" ]]; then
        _notset_input_key=1
        [[ -n "$_input_default" ]] && _notset_input_key=2
        [[ $DEBUG -gt 0 ]] && echo "Found empty input; _notset_input_key=$_notset_input_key"
    else
        # %q - print the associated argument shell-quoted, reusable as input
        _read_input_key=$(printf "%q" "$_read_input_key")
    fi

    # if empty set variable to default value
    if [[ $_notset_input_key -eq 2 ]]; then
        [[ $DEBUG -gt 0 ]] && echo "_input_key="$_input_default
        eval "$_input_key="$_input_default
    else
        eval "$_input_key="$_read_input_key
        [[ $DEBUG -gt 0 ]] && echo "_input_key="$_read_input_key
    fi
    echo
}
# print menu
print_menu(){
    IFS_BAK=$IFS
    IFS=$'\n'
    echo "Available actions"
    for menu_item in $menu_list; do
        echo -e "\t\t" $menu_item
    done
    IFS=$IFS_BAK
    IFS_BAK=
}

binom_monitoring(){
for n in {1..2}; do
menuenabled=`cat /opt/binomload/data/info/menuenabled`
enmontsung=`cat /opt/binomload/data/info/enmontsung`
enmonbinom=`cat /opt/binomload/data/info/enmonbinom`
enmonlog=`cat /opt/binomload/data/info/enmonlog`

if [[ ${menuenabled} = 1 ]]; then
sleep 0
echo 0 > /opt/binomload/data/info/menuenabled
else
if [ "$enmontsung" = "1" -o "$enmonbinom" = "1" -o "$enmonlog" = 1 ]; then

while true
do
echo 1 > /opt/binomload/data/info/menuenabled
monrestart=`cat /opt/binomload/data/info/monrestart`
montsung=`cat /opt/binomload/data/info/montsung`
statustsung=`tsung status`

if [[ ${monrestart} = 1 ]]; then
echo 0 > /opt/binomload/data/info/monrestart
break
else
echo -e "\n"
echo -e "\n"
echo -e "\n"
echo -e "\n"
echo -e "\n"
echo -e "\n"
echo -e "\n"
echo -e "\n"

###Tsung Mon###
if [[ ${enmontsung} = 0 ]]; then
sleep 0
else
echo -ne "TSUNG STATUS: ${statustsung} \n"
echo 1 > /opt/binomload/data/info/menuenabled
pstsung=`ps -auxf | grep "SCREEN -d -m tsung" | grep -v grep | awk '{print $14}'`
fi

### Binom Mon###
if [[ ${enmonbinom} = 0 ]]; then
sleep 0
else
urlbinommon=`cat /opt/binomload/data/info/urlmonbinom`
idbinommon=`cat /opt/binomload/data/info/idmonbinom`
monbinom=`curl -s "$urlbinommon" | sed 's|"| " |g' | sed 's|,|\n|g' | grep -e' id \|clicks' | sed 's|{||g' | grep -w "$idbinommon" -A 1`
echo -ne "Binom-Monitoring STATUS: ${monbinom} \n"
echo 1 > /opt/binomload/data/info/menuenabled
fi
if [[ ${enmonlog} = 0 ]]; then
sleep 0
else
tsunglog=`ls -latr /opt/binomload/logs/tsung/ | tail -1 | awk '{print $9}'`
montsunglog=`grep "stats: 302" /opt/binomload/logs/tsung/${tsunglog}/tsung.log | tail -1 `
echo -ne "Tsung-Clicks STATUS: ${montsunglog} \n"
echo 1 > /opt/binomload/data/info/menuenabled
fi
if [[ ${pstsung} = tsung ]]; then
echo -e "\e[2A"
echo -e "\e[2A"
echo -e "\e[2A"
echo -e "\e[2A"
echo 1 > /opt/binomload/data/info/menuenabled
fi

echo -e "\e[4A"
if [[ ${enmontsung} = 1 ]]; then
echo -e "\e[2A"
echo -e "\e[2A"
echo -e "\e[2A"
fi
if [[ ${enmonbinom} = 1 ]]; then
echo -e "\e[2A"
echo -e "\e[2A"
fi
if [[ ${enmonlog} = 1 ]]; then
echo -e "\e[2A"
fi
echo -e "\e[2A"
echo -e "\e[2A"
echo -e "\e[2A"
echo -e "\e[2A"

echo -e "\e[2A"
echo -e "\e[2A"
echo -e "\e[2A"
echo -e "\e[2A"
echo -e "\e[2A"
echo -e "\e[2A"
echo -e "\e[2A"

sleep 5
fi
done &

fi
fi
done
}