#!/bin/bash
export LANG=en_US.UTF-8
export TERM=linux
PROGNAME=$(basename $0)
PROGPATH=/opt/binomload/bin
VERBOSE=0
BASE_DIR=/opt/binomload
LOGS_DIR=$BASE_DIR/logs
TEMP_DIR=$BASE_DIR/temp
LOGS_FILE=$LOGS_DIR/pool_menu.log
[[ -z $DEBUG ]] && DEBUG=0

. $PROGPATH/binom_utils.sh || exit 1

logo=$(get_logo)

# a_menu="0. Выбор url-а.";
a_pick(){
  $PROGPATH/menu/seturl_tsung.sh || exit 1
}

#  b_menu="1. Создания задания нагрузки."
b_pick(){
  $PROGPATH/menu/setload_tsung.sh || exit 1
}

# c_menu="2. Настройка User-agent"
c_pick(){
  $PROGPATH/menu/uaset_tsung.sh || exit 1
}
# d_menu="3. Настройка ip-адресов"
d_pick(){
  $PROGPATH/menu/ipset_tsung.sh || exit 1
}
#e_menu="4. Настройка токенов"
e_pick(){
  $PROGPATH/menu/tockenset_tsung.sh || exit 1
}
s_pick(){
hostts=`cat /opt/binomload/data/build/host`
urlsts=`cat /opt/binomload/data/build/urls`
users=`cat ${BASE_DIR}/data/build/users`
inter=`cat ${BASE_DIR}/data/build/interval`
useri=`cat ${BASE_DIR}/data/build/usersint`
durat=`cat ${BASE_DIR}/data/build/duration`

  echo -e "\t\t" `print_color_text "Активный host ${hostts}" green -n`
  echo -e "\t\t" `print_color_text "Активные urls ${urlsts}" green -n`
  echo -e "\t\t" `print_color_text "Активно ${users} пользователей кажду ${useri}" green -n`
  echo -e "\t\t" `print_color_text "Длительность нагрузки в течении ${durat} ${inter} " green -n`

echo -n "Продолжить? (y/N) "

read item
case "$item" in

    y|Y) echo "Ввели «y», продолжаем..."

echo -e "\t\t" "TestBinom01"
read -rp "Введите значение: " namexml
echo "Вы ввели: $namexml!"
echo "${namexml}-${hostts}-${users}-${useri}-${durat}-${inter}.xml" > ${BASE_DIR}/data/build/namexml
echo "Спасибо! Название XML-файла - ${namexml}-${hostts}-${users}-${useri}-${durat}-${inter}.xml"
sleep 2

  $PROGPATH/script/build_tsungxml.sh

        ;;
    n|N) echo "Ввели «n», завершаем..."
        $PROGPATH/menu/settingload_menu.sh || exit 1
        ;;
    *) echo "Ничего не ввели. Выполняем действие по умолчанию..."
        ;;
esac
}

#g_menu="5. Вернуться в предыдущее меню"
g_pick(){
  $PROGPATH/binom_load.sh || exit 1
}

print_menu(){
  # menu options
  a_menu="1. Настройка url-ов \ Useragent \ IP \ Tockens";
  b_menu="2. Создания задания нагрузки";
  s_menu="10. Сборка Tsung-файла"
  g_menu="0. Вернуться в главное меню";
# print menu
  LOGO=$(get_logo)
  #IP=$(get_ip_addr)
  echo
  echo -e "\t\t" `print_color_text "${LOGO}" cyan -n`
  echo
  echo -e "\t\t" `print_color_text "Выбор действий:" green -n`
  echo

  echo -e "\t\t" `print_color_text "${a_menu}" blue -n`
  echo -e "\t\t" `print_color_text "${b_menu}" blue -n`
  echo -e "\t\t" `print_color_text "${s_menu}" red -n`
  echo -e "\t\t" `print_color_text "${g_menu}" magenta -n`



  echo

  echo $notice_message
 echo `print_color_text "(Ctrl-C что бы выйти из скрипта)" red -n`
  echo `print_color_text "Выберите номер и нажмите ENTER:" green -n` ;
  notice_message=""
}
set +x
while true; do
  clear;
  print_menu
  read user_choice
  # processing choice
  case $user_choice in
    1|a) a_pick;;
    2|b) b_pick;;
    3|c) c_pick;;
    4|d) d_pick;;
    5|e) e_pick;;
    10|s) s_pick;;
    0|g) g_pick;;
  *)   error_pick;;
  esac
done
