#!/bin/bash
export LANG=en_US.UTF-8
export TERM=linux
PROGNAME=$(basename $0)
PROGPATH=/opt/binomload/bin
VERBOSE=0
BASE_DIR=/opt/binomload
LOGS_DIR=$BASE_DIR/logs
TEMP_DIR=$BASE_DIR/temp
LOGS_FILE=$LOGS_DIR/pool_menu.log
enmontsung=`cat /opt/binomload/data/info/enmontsung`
[[ -z $DEBUG ]] && DEBUG=0

. $PROGPATH/binom_utils.sh || exit 1

logo=$(get_logo)

# a_menu="0. Статус tsung.";
a_pick(){
  tsung status && sleep 3
}

#  b_menu="1. Выбор конфига."
b_pick(){
if [[ ${enmontsung} = 0 ]]; then
echo 1 > /opt/binomload/data/info/enmontsung
echo 1 > /opt/binomload/data/info/monrestart
else
echo 0 > /opt/binomload/data/info/enmontsung
echo 1 > /opt/binomload/data/info/monrestart
fi
}

# c_menu="2. enable tsung status in script"
c_pick(){
enmonbinom=`cat /opt/binomload/data/info/enmonbinom`
if [[ ${enmonbinom} = 0 ]]; then
echo 1 > /opt/binomload/data/info/enmonbinom
echo -e "\t\t" "Мониторинг включен"
echo -n "Настроить?? (y/N) "

read item
case "$item" in

    y|Y) echo "Ввели «y», продолжаем..."
read -rp "Введите полный путь с api_key: " urlapikey
echo "Вы ввели: $urlapikey"
echo $urlapikey > /opt/binomload/data/info/urlmonbinom
read -rp "Номер кампании: " campid
echo "Вы ввели id камппаннии $campid"
echo $campid >  /opt/binomload/data/info/idmonbinom
	;;
    n|N) echo "Ввели «n», завершаем..."
        sleep 3
        ;;
    *) echo "Ничего не ввели. Выполняем действие по умолчанию..."
        sleep 3 
        ;;
esac

echo 1 > /opt/binomload/data/info/monrestart

else
echo "Статистика отключена"
echo 1 > /opt/binomload/data/info/monrestart
echo 0 > /opt/binomload/data/info/enmonbinom
sleep 2
fi
}
e_pick(){
enmonlog=`cat /opt/binomload/data/info/enmonlog`
if [[ ${enmonlog} = 1 ]]; then
echo 0 > /opt/binomload/data/info/enmonlog
echo 1 > /opt/binomload/data/info/monrestart
else
echo 1 > /opt/binomload/data/info/monrestart
echo 1 > /opt/binomload/data/info/enmonlog
fi
}
d_pick(){
  $PROGPATH/binom_load.sh || exit 1
}

print_menu(){
  # menu options
  enmontsung=`cat /opt/binomload/data/info/enmontsung`
  enstatlogtsung=`cat /opt/binomload/data/info/enstatlogtsung`
  enmonlog=`cat /opt/binomload/data/info/enmonlog`
  enmonbinom=`cat /opt/binomload/data/info/enmonbinom`
  a_menu="1. Статуc Tsung.";
  if [[ ${enmontsung} = 0 ]]; then
  b_menu="2. Включение статуса tsung в скрипте.";
  else
  b_menu="2. Вылючение статуса tsung в скрипте.";
  fi
  if [[ ${enmonbinom} = 0 ]]; then
  c_menu="3. Сбор статистики с трекера по api_key";
  else
  c_menu="3. Отключение статистики с трекера по api_key";
  fi
  if [[ ${enmonlog} -ne 0 ]]; then
  e_menu="4. Выключение статистики с лог файла";
  else
  e_menu="4. Включение статистики с лог файла";
  fi
  d_menu="0. Вернуться в главное меню";
# print menu
  LOGO=$(get_logo)
  #IP=$(get_ip_addr)

   echo
  echo -e "\t\t" `print_color_text "${LOGO}" cyan -n`
  echo
  echo -e "\t\t" `print_color_text "Выбор действий:" green -n`
  echo
  echo -e "\t\t" `print_color_text "${a_menu}" blue -n`
  echo -e "\t\t" `print_color_text "${b_menu}" red -n`
  echo -e "\t\t" `print_color_text "${c_menu}" red -n`
  echo -e "\t\t" `print_color_text "${e_menu}" red -n`
  echo -e "\t\t" `print_color_text "${d_menu}" magenta -n`
  echo

  echo $notice_message
    echo `print_color_text "(Ctrl-C что бы выйти из скрипта)" red -n`
  echo `print_color_text "Выберите номер и нажмите ENTER:" green -n` ;
  notice_message=""
}
set +x
while true; do
  clear;

  print_menu
  read user_choice
  # processing choice
  case $user_choice in
    1|a) a_pick;;
    2|b) b_pick;;
    3|c) c_pick;;
    4|e) e_pick;;
    0|d) d_pick;;
  *)   error_pick;;
  esac
done

