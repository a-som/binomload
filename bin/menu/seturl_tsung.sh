#!/bin/bash
export LANG=en_US.UTF-8
export TERM=linux
PROGNAME=$(basename $0)
PROGPATH=/opt/binomload/bin
VERBOSE=0
BASE_DIR=/opt/binomload
LOGS_DIR=$BASE_DIR/logs
TEMP_DIR=$BASE_DIR/temp
LOGS_FILE=$LOGS_DIR/pool_menu.log
[[ -z $DEBUG ]] && DEBUG=0

. $PROGPATH/binom_utils.sh || exit 1

logo=$(get_logo)

# a_menu="1. Добавление host";
a_pick(){
echo -e "\t\t" "Введите пожалуйста host без http"
echo -e "\t\t" "Например test.binom.org"
read -rp "Введите host: " hosttsung
echo "Вы ввели: $hosttsung! Спасибо!"
echo "$hosttsung" > /opt/binomload/data/build/host
sleep 2
}

#  b_menu="2. Добавленение Url"
b_pick(){
urls=`cat /opt/binomload/data/build/urls`
urlsnum=`cat /opt/binomload/data/build/urls | wc -l`
echo -e "\t\t" "Сейчас добавлено $urlsnum urls"
echo -e "\t\t" "Вы хотите добавить новые urls или создать полностью новый файл?"
echo -e "Введите 1 если хотите добавить url к нынешнему файлу (1/y)"
echo -e "Введите 2 если хотите добавить url к новому файлу (2/f)"
echo -e "Введите 1 если хотите добавить url к нынешнему файлу (3/n)"
read item
case "$item" in

    1|y|Y) echo "Ввели add to file «y», продолжаем..."
    echo -e "\t\t" "Введите пожалуйста путь url без host"
echo -e "\t\t" "Например /binom/click.php?camp_id=1&key=l4969a8wpps04ea46khm&token1={token1}"
read -rp "Введите url: " urltsung
echo "Вы ввели: $urltsung! Спасибо!"
echo "$urltsung" >> /opt/binomload/data/build/urls
sleep 2
        ;;
    2|f|F) echo "Ввели NewFile «f», продолжаем..."
       echo -e "\t\t" "Введите пожалуйста путь url без host"
echo -e "\t\t" "Например /binom/click.php?camp_id=1&key=l4969a8wpps04ea46khm&token1={token1}"
read -rp "Введите url: " urltsung
echo "Вы ввели: $urltsung! Спасибо!"
echo "$urltsung" > /opt/binomload/data/build/urls
sleep 2
	;;
    n|N|3) echo "Ввели «n», завершаем..."
        $PROGPATH/menu/settingload_menu.sh || exit 1
        ;;
    *) echo "Ничего не ввели. Выполняем действие по умолчанию..."
        ;;
esac



}

# c_menu="3. Добавление несколько Urls"
 c_menu="2. Настройка User-agent"
c_pick(){
typeua=`cat /opt/binomload/data/build/typeua`
echo -e "Введите 1 если хотите добавить 1 UserAgent (1/a)"
echo -e "Введите 2 если хотите добавить несколько UserAgent (2/b)"
echo -e "Введите 3 еслих хотите использовать файл случайных UserAgent-ов 3/c"
echo -e "Введите N если не хотите ничего добавлять"
echo -e "Сейчас используется тип $typeua"
read item
case "$item" in
1|a|A) echo "Ввели 1 UserAgent продолжаем..."
echo -e "\t\t" "Введите пожалуйста UserAgent:"
read urlua1
echo "Вы ввели: $urlua1! Спасибо!"
echo 1 > /opt/binomload/data/build/typeua
echo "$urlua1" > /opt/binomload/data/build/urlua1
        sleep 2
        ;;
2|b|B) echo "Ввели 2 Несколько UserAgents, продолжаем..."
echo -e "\t\t" "Введите пожалуйста UserAgent:"
read urlua2
echo -e "Спасибо. Вы ввели  $urlua2"
sleep 2
if [[ "$typeua" = 2 ]]; then
echo "$urlua2" >>  /opt/binomload/data/build/urlua2
else
echo 2 > /opt/binomload/data/build/typeua
echo "$urlua2" >  /opt/binomload/data/build/urlua2
fi
        ;;
3|c|C) echo "Ввели 3 - Файл случайных ua "
echo 3 > /opt/binomload/data/build/typeua
sleep 2    
       ;;
n|N|4) echo "Ввели «n», завершаем..."
        $PROGPATH/menu/settingload_menu.sh || exit 1
        ;;
    *) echo "Ничего не ввели. Выполняем действие по умолчанию..."
        ;;
esac
}
# d_menu="3. Настройка ip-адресов"
d_pick(){
echo -e "Введите 1 если хотите добавить 1 IP (1/a)"
echo -e "Введите 2 если хотите добавить несколько IP (2/b)"
echo -e "Введите 3 еслих хотите использовать файл случайных IP-адресов с выбраным кол-вом айпи (~до 10000) (3/c)"
echo -e "Введите N если не хотите ничего добавлять"
echo -e "Сейчас используется тип $typeip"
read item
case "$item" in
1|a|A) echo "Ввели 1 IP продолжаем..."
echo -e "\t\t" "Введите пожалуйста UserAgent:"
read -rp urlip1
echo "Вы ввели: $urlip1! Спасибо!"
echo 1 > /opt/binomload/data/build/typeip
echo "$urlip1" > /opt/binomload/data/build/urlip1
        sleep 2
        ;;
2|b|B) echo "Ввели 2 Несколько UserAgents, продолжаем..."
echo -e "\t\t" "Введите пожалуйста UserAgent:"
read -rp urlip2
echo -e "Спасибо. Вы ввели  $urlip2"
sleep 2
if [[ "$typeua" = 2 ]]; then
echo "$urlip2" >>  /opt/binomload/data/build/urlip2
else
echo 2 > /opt/binomload/data/build/typeip
echo "$urlip2" >  /opt/binomload/data/build/urlip2
fi
        ;;
3|b|B) echo "Ввели 3 - Файл случайных ip. Максимум 10000 "
echo 3 > /opt/binomload/data/build/typeip
echo -e "\t\t Введите кол-во:"
echo -e "\n"
read numip
shuf -n $numip /opt/binomload/data/gen/ips.txt > /opt/binomload/data/gen/iprand.txt 
echo "Файл сгенерирован. Спасибо!"
sleep 2
       ;;
n|N|4) echo "Ввели «n», завершаем..."
        $PROGPATH/menu/settingload_menu.sh || exit 1
        ;;
    *) echo "Ничего не ввели. Выполняем действие по умолчанию..."
        ;;
esac
}

#e_menu="4. Настройка токенов DEV"
e_pick(){
  $PROGPATH/menu/tockenset_tsung.sh || exit 1
}
x_menu(){
echo -e "\t\t" "Генерация нового файла urls. Вы уверены в этом?"
echo -e "\t\t" "Сейчас есть:"
echo -e "\t\t Сервер:" $hostts
echo -e "\t\t URLS:" $urlsts
echo -e "\t\t UA кол-во" `cat /opt/binomload/data/build/urlua${typeua} | wc -l`
echo -e "\t\t IP кол-во" `cat /opt/binomload/data/build/urlip${typeip} | wc -l`
echo -e "\t\t y\Y если да. N\n если нет"
read item
case "$item" in

    y|Y) echo "Ввели «y», продолжаем..."
	$PROGPATH/script/urlconfigbuild.sh
    echo "Файл urls сгенерирован. Можете приступить к настройке задания."
    sleep 2
	;;
    n|N) echo "Ввели «n», завершаем..."
        $PROGPATH/menu/settingload_menu.sh || exit 1
        ;;
    *) echo "Ничего не ввели. Выполняем действие по умолчанию..."
        ;;
esac
}
# d_menu="4. Вернуться в предыдущее меню"
f_pick(){
  $PROGPATH/menu/settingload_menu.sh || exit 1
}
g_pick(){
  $PROGPATH/binom_load.sh || exit 1
}
print_menu(){
  # menu options
  a_menu="1. Добавление Host.";
  b_menu="2. Добавление URL-s";
  c_menu="3. Настройка User-agent";
  d_menu="4. Настройка ip-адресов";
  e_menu="5. Настройка токенов";
  x_menu="10. Собрать url-конфиг ";
  
  f_menu="0. Выйти в предыдущее меню";
  g_menu="00. Вернуться в главное меню";
# print menu
  LOGO=$(get_logo)
  #IP=$(get_ip_addr)
  hostts=`cat /opt/binomload/data/build/host`
  urlsts=`cat /opt/binomload/data/build/urls`
  typeua=`cat /opt/binomload/data/build/typeua`
  typeip=`cat /opt/binomload/data/build/typeip`
  echo
  echo -e "\t\t" `print_color_text "${LOGO}" cyan -n`
  echo
  echo -e "\t\t" `print_color_text "Выбор действий:" green -n`
  echo
  echo -e "\t\t" `print_color_text "${a_menu}"  blue -n`
  echo -e "\t\t" `print_color_text "Активный host ${hostts}" green -n`
  echo -e "\t\t" `print_color_text "${b_menu}"  blue -n`
  echo -e "\t\t" `print_color_text "Активные urls" green -n`
  echo -e "\t\t" `print_color_text "${urlsts}" green -n`
  echo -e "\t\t" `print_color_text "${c_menu}" blue -n`
  echo -e "\t\t" `print_color_text "Активный TYPE $typeua" green -n`
  echo -e "\t\t" `print_color_text "${d_menu}" blue -n`
  echo -e "\t\t" `print_color_text "Активный TYPE $typeip" green -n`
  echo -e "\t\t" `print_color_text "${e_menu}" blue -n`
  echo -e "\t\t" `print_color_text "${x_menu}" red -n`
  echo -e "\t\t" `print_color_text "${f_menu}" magenta -n`
  echo -e "\t\t" `print_color_text "${g_menu}" magenta -n`
  echo
  echo $notice_message
  echo `print_color_text "(Ctrl-C что бы выйти из скрипта)" red -n`
  echo `print_color_text "Выберите номер и нажмите ENTER:" green -n` ;
  notice_message=""

}

while true; do
  clear;
  print_menu
  read user_choice
  # processing choice
  case $user_choice in
    1|a) a_pick;;
    2|b) b_pick;;
    3|c) c_pick;;
    4|d) d_pick;;
    5|e) e_pick;;
    10|x) x_pick;;
    0|f) f_pick;;
    00|g) g_pick;;
  *)   error_pick;;
  esac
done