#!/bin/bash
export LANG=en_US.UTF-8
export TERM=linux
PROGNAME=$(basename $0)
PROGPATH=/opt/binomload/bin
VERBOSE=0
BASE_DIR=/opt/binomload
LOGS_DIR=$BASE_DIR/logs
TEMP_DIR=$BASE_DIR/temp
LOGS_FILE=$LOGS_DIR/pool_menu.log
[[ -z $DEBUG ]] && DEBUG=0

. $PROGPATH/binom_utils.sh || exit 1

logo=$(get_logo)

# a_menu="1. Добавление Очереди кликов в секунду";
a_pick(){
echo -e "\t\t" "Пожалуйста введите кол-во новых кликов в секунду\минуту"
echo -e "\t\t" "Например 100"
read -rp "Введите кол-во: " numusers
echo "Вы ввели: $numusers!"
echo "$numusers" > ${BASE_DIR}/data/build/users
echo -e "\t\t" "Пожалуйста введите интервал новых кликов в секунду\минуту"
echo -e "\t\t" "Например second / minute"
read -rp "Введите кол-во: " usersint
echo "Вы ввели: $usersint!"
echo "$usersint" > ${BASE_DIR}/data/build/usersint

echo "Спасибо! Вы ввели добавление $numusers каждую $usersint"


echo -e "\t\t" "Теперь пожалуйста введите продолжительность первой фазы"
echo -e "\t\t" "Пожалуйста введите длительность этого периода"
echo -e "\t\t" "Например 10"
read -rp "Введите значение: " duration
echo "Вы ввели: $duration!"
echo "$duration" > ${BASE_DIR}/data/build/duration

echo -e "\t\t" "Например second \ minute"
read -rp "Введите значение: " interval
echo "Вы ввели: $interval!"
echo "$interval" > ${BASE_DIR}/data/build/interval
echo "Спасибо! Вы ввели продолжительность первой фазы в $interval $duration"
sleep 2
}

#  b_menu="2. Собрать в config-file"
b_pick(){
$PROGPATH/script/loadconfigbuild.sh
echo "Конфигурационый файл для пользователей собран."
echo "Добавлено задание:"
echo -e `cat ${BASE_DIR}/data/build/users` "пользователей каждую " `cat ${BASE_DIR}/data/build/usersint`
echo -e "В течении" `cat ${BASE_DIR}/data/build/interval` `cat ${BASE_DIR}/data/build/duration`
sleep 5
}

# d_menu="4. Вернуться в предыдущее меню"
d_pick(){
  $PROGPATH/menu/settingload_menu.sh || exit 1
}
g_pick(){
  $PROGPATH/binom_load.sh || exit 1
}
print_menu(){
  # menu options
  a_menu="1. Добавление Очереди кликов в секунду.";
  b_menu="2. Собрать в config-file";
  d_menu="0. Выйти в предыдущее меню";
  g_menu="00. Вернуться в главное меню";
# print menu
  LOGO=$(get_logo)
  #IP=$(get_ip_addr)
users=`cat ${BASE_DIR}/data/build/users`
inter=`cat ${BASE_DIR}/data/build/interval`
useri=`cat ${BASE_DIR}/data/build/usersint`
durat=`cat ${BASE_DIR}/data/build/duration`
  echo
  echo -e "\t\t" `print_color_text "${LOGO}" cyan -n`
  echo
  echo -e "\t\t" `print_color_text "Выбор действий:" green -n`
  echo
  echo -e "\t\t" `print_color_text "${a_menu}" blue -n`
  echo -e "\t\t" `print_color_text "Активно ${users} пользователей кажду ${useri}" green -n`
  echo -e "\t\t" `print_color_text "Длительность нагрузки в течении ${durat} ${inter} " green -n`
  echo -e "\t\t" `print_color_text "${b_menu}" red -n`
  echo -e "\t\t" `print_color_text "${d_menu}" magenta -n`
  echo -e "\t\t" `print_color_text "${g_menu}" magenta -n`



  echo

  echo $notice_message
  echo `print_color_text "(Ctrl-C что бы выйти из скрипта)" red -n`
  echo `print_color_text "Выберите номер и нажмите ENTER:" green -n` ;
  notice_message=""
}
set +x
while true; do
  clear;
  print_menu
  read user_choice
  # processing choice
  case $user_choice in
    1|a) a_pick;;
    2|b) b_pick;;
    0|d) d_pick;;
    00|g) g_pick;;
  *)   error_pick;;
  esac
done