#!/bin/bash
export LANG=en_US.UTF-8
export TERM=linux
PROGNAME=$(basename $0)
PROGPATH=/opt/binomload/bin
VERBOSE=0
BASE_DIR=/opt/binomload
LOGS_DIR=$BASE_DIR/logs
TEMP_DIR=$BASE_DIR/temp
LOGS_FILE=$LOGS_DIR/pool_menu.log
[[ -z $DEBUG ]] && DEBUG=0

. $PROGPATH/binom_utils.sh || exit 1

logo=$(get_logo)

# a_menu="0. Старт tsung.";
a_pick(){
  $PROGPATH/script/tsung_start.sh && sleep 1 && echo "Здесь будет проверка статуса tsung. Пока, что нету, но 99% что он запущен" && sleep 2
}
e_pick(){
  tsung stop && sleep 1
}
#  b_menu="1. Выбор конфига."
b_pick(){
unset options i
while IFS= read -r -d $'\0' f; do
  options[i++]="$f"
done < <(find /opt/binomload/data/tsungconfig/ -maxdepth 1 -type f -name "*.xml" -print0 )

select opt in "${options[@]}" "Не выбирать ничего"; do
  case $opt in
    *.xml)
      echo "xml Файл $opt был выбран как активный"
      echo "$opt" > /opt/binomload/data/info/activexml
      $PROGPATH/menu/loadstart_menu.sh && sleep 1 || exit 1
      # processing
      ;;
    "Не выбирать ничего")
      echo "Вы выбрали не выбирать"
      break
      ;;
    *)
      echo "Это не число"
      ;;
  esac
done
}

# c_menu="2. Статус tsung"
c_pick(){
   tsung status && sleep 3
}
d_pick(){
  $PROGPATH/binom_load.sh || exit 1
}

print_menu(){
  # menu options
  a_menu="1. Старт tsung.";
  b_menu="2. Выбор конфига";
  c_menu="3. Статус tsung";
  e_menu="4. Остановить tsung";
  d_menu="0. Вернуться в главное меню";
# print menu
  LOGO=$(get_logo)
  #IP=$(get_ip_addr)
  xml=`cat /opt/binomload/data/info/activexml`
  echo
  echo -e "\t\t" `print_color_text "${LOGO}" cyan -n`
  echo
  echo -e "\t\t" `print_color_text "Выбор действий:" green -n`
  echo

  echo
  echo -e "\t\t" `print_color_text "${a_menu}" red -n`
  echo -e "\t\t" `print_color_text "${b_menu}" blue -n`
  echo -e "\t\t" `print_color_text "Активный ${xml}" green -n`
  echo -e "\t\t" `print_color_text "${c_menu}" blue -n`
  echo -e "\t\t" `print_color_text "${e_menu}" red -n`
  echo -e "\t\t" `print_color_text "${d_menu}" magenta -n`
  echo
  
  echo $notice_message
    echo `print_color_text "(Ctrl-C что бы выйти из скрипта)" red -n`
  echo `print_color_text "Выберите номер и нажмите ENTER:" green -n` ;
  notice_message=""
}
set +x
while true; do
  clear;

  print_menu
  read user_choice
  # processing choice
  case $user_choice in
    1|a) a_pick;;
    2|b) b_pick;;
    3|c) c_pick;;
    4|e) e_pick;;
    0|d) d_pick;;
  *)   error_pick;;
  esac
done

