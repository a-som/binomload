#!/bin/bash
export LANG=en_US.UTF-8
export TERM=linux
PROGNAME=$(basename $0)
PROGPATH=/opt/binomload/bin	
VERBOSE=0
BASE_DIR=/opt/binomload
LOGS_DIR=$BASE_DIR/logs
TEMP_DIR=$BASE_DIR/temp
LOGS_FILE=$LOGS_DIR/pool_menu.log
[[ -z $DEBUG ]] && DEBUG=0

. $PROGPATH/binom_utils.sh || exit 1

logo=$(get_logo)

# a_menu="0. Меню старта.";
a_pick(){
  $PROGPATH/menu/loadstart_menu.sh || exit 1
}

#  b_menu="1. Сборка и настройка конфига."
b_pick(){
  $PROGPATH/menu/settingload_menu.sh || exit 1 
}

# c_menu=2. Статус tsung и других сервисов."
c_pick(){
  $PROGPATH/menu/status_menu.sh || exit 1
}
print_menu(){
  # menu options

  a_menu="1. Меню старта tsung.";
  b_menu="2. Меню сборки и настройки конфигурации";
  c_menu="3. Опции скрипта и мониторинга";
  # print menu
  LOGO=$(get_logo)
  #IP=$(get_ip_addr)

  echo
  echo -e "\t\t" `print_color_text "${LOGO}" cyan -n`
  echo
  echo -e "\t\t" `print_color_text "Выбор действий:" green -n`
  echo 
  echo -e "\t\t" `print_color_text "${a_menu}" red -n`
  echo -e "\t\t" `print_color_text "${b_menu}" magenta -n`
  echo -e "\t\t" `print_color_text "${c_menu}" magenta -n`
  echo
  echo $notice_message
  echo `print_color_text "(Ctrl-C что бы выйти из скрипта)" red -n`
  echo `print_color_text "Выберите номер и нажмите ENTER:" green -n` ;
  notice_message=""

}
binom_monitoring
while true; do
clear;

  print_menu
  read user_choice
  # processing choice
  case $user_choice in
    1|a) a_pick;;
    2|b) b_pick;;
    3|c) c_pick;;
    *)   error_pick;;
  esac
done
