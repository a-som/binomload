#!/bin/bash
BASE_DIR=/opt/binomload
dur=`cat ${BASE_DIR}/data/build/duration`
unit=`cat ${BASE_DIR}/data/build/interval`
users=`cat ${BASE_DIR}/data/build/users`
userdur=`cat ${BASE_DIR}/data/build/usersint`

echo -e '<arrivalphase phase="1" duration="'${dur}'" unit="'${unit}'">' > ${BASE_DIR}/data/build/arrivalphase
echo -e '    <users arrivalrate="'${users}'" unit="'${userdur}'"></users>' >> ${BASE_DIR}/data/build/arrivalphase
echo '   </arrivalphase>' >> ${BASE_DIR}/data/build/arrivalphase

