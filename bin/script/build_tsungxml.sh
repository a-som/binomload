#!/bin/bash
BASE_DIR=/opt/binomload
hostts=`cat ${BASE_DIR}/data/build/host`
sed 's|&|&#038;|g' ${BASE_DIR}/data/build/urls > ${BASE_DIR}/data/build/url-xml
urlsts=`cat ${BASE_DIR}/data/build/url-xml`
namexml=`cat ${BASE_DIR}/data/build/namexml`


echo '<?xml version="1.0"?>
<!DOCTYPE tsung SYSTEM "/usr/share/tsung/tsung-1.0.dtd">
<tsung loglevel="debug" version="1.0"  dumptraffic="true">

  <!-- Client side setup -->
  <clients>
    <client host="localhost" use_controller_vm="true" maxusers="15000"/>
  </clients>
  <!-- Server side setup -->
<servers>
  <server host="'$hostts'" port="80" type="tcp"></server>
</servers>

  <load>
  <!-- several arrival phases can be set: for each phase, you can set
  the mean inter-arrival time between new clients and the phase
  duration -->' > ${BASE_DIR}/data/tsungconfig/${namexml}
cat "/opt/binomload/data/build/arrivalphase" >> ${BASE_DIR}/data/tsungconfig/${namexml}
echo -e '</load>'  >> ${BASE_DIR}/data/tsungconfig/${namexml}
cat "/opt/binomload/data/build/urlsbuild" >> ${BASE_DIR}/data/tsungconfig/${namexml}
echo -e '</tsung>' >> ${BASE_DIR}/data/tsungconfig/${namexml}