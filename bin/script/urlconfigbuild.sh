#!/bin/bash
BASE_DIR="/opt/binomload"
BUILD_DIR="/opt/binomload/data/build"
urlsnum=`cat ${BUILD_DIR}/urls | wc -l`
hostts=`cat ${BUILD_DIR}/host`
typeua=`cat ${BUILD_DIR}/typeua`
typeip=`cat ${BUILD_DIR}/typeip`
pwgen -A 8 1 > $BUILD_DIR/dataxml
sed 's|&|&#038;|g' ${BASE_DIR}/data/build/urls > ${BASE_DIR}/data/build/url-xml
dataxml=`cat $BUILD_DIR/dataxml`
dirdataxml="/opt/binomload/data/dataxml/${dataxml}"
mkdir -p $dirdataxml
cp /opt/binomload/data/gen/langs.txt $dirdataxml/lang.txt
cp /opt/binomload/data/gen/tokens.txt $dirdataxml/tockens.txt
if [[ $typeua = 3 ]]; then
cp /opt/binomload/data/gen/uas.txt ${dirdataxml}/uas.txt
else
cp /opt/binomload/data/gen/urlua${typeua} ${dirdataxml}/uas.txt
fi
if [[ $typeip = 3 ]]; then
cp /opt/binomload/data/gen/iprand.txt $dirdataxml/ips.txt
else
cp /opt/binomload/data/gen/urlip${typeip} ${dirdataxml}/ips.txt
fi
echo '<options>' > ${BUILD_DIR}/urlsbuild
echo -e '<option name="file_server" id="lang_list" value="'$dirdataxml/lang.txt'"></option>' >> ${BUILD_DIR}/urlsbuild
echo -e '<option name="file_server" id="tokens_list" value="'$dirdataxml/tockens.txt'"></option>' >> ${BUILD_DIR}/urlsbuild
echo -e '<option name="file_server" id="useragent_list" value="'${dirdataxml}/uas.txt'"></option>' >> ${BUILD_DIR}/urlsbuild
echo -e '<option name="file_server" id="ip_list" value="'${dirdataxml}/ips.txt'"></option>' >> ${BUILD_DIR}/urlsbuild
echo -e '</options>' >> ${BUILD_DIR}/urlsbuild
echo -e ' <sessions>
   <session name="http-example" probability="100" type="ts_http">

    <!-- full url with server name, this overrides the "server" config value -->
 <setdynvars sourcetype="file" fileid="useragent_list" delimiter=":" order="random">
                 <var name="user_agent" />
              </setdynvars>
 <setdynvars sourcetype="file" fileid="ip_list" delimiter=";" order="random">
                 <var name="ip_ran" />
              </setdynvars>
 <setdynvars sourcetype="file" fileid="lang_list" delimiter=":" order="random">
                 <var name="lang_list" />
              </setdynvars>
 <setdynvars sourcetype="file" fileid="tokens_list" delimiter=";" order="random">
                 <var name="token_rand" />
              </setdynvars>' >> ${BUILD_DIR}/urlsbuild
x=1
while [ $x -le $urlsnum ]
do
  echo "Add $x times"
urlnumline=`cat "${BASE_DIR}/data/build/url-xml" | head -$x | tail -1`
echo -e '<request subst="true">' >> ${BUILD_DIR}/urlsbuild
echo -e '<http url="'${urlnumline}'" method="GET" version="1.1">' >> ${BUILD_DIR}/urlsbuild
echo -e '<http_header name="User-agent" value="%%_user_agent%%" />
         <http_header name="X-Forwarded-For" value="%%_ip_ran%%" />
         <http_header name="Accept-Language" value="%%_lang_list%%" />
         <http_header name="Via" value="1.1" />
        </http>
    </request>
     <thinktime value="2" random="true"></thinktime>
   ' >> ${BUILD_DIR}/urlsbuild
x=$(( $x + 1 ))
done
echo -e '</session>
</sessions>' >> ${BUILD_DIR}/urlsbuild